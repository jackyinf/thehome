import { injectReducer } from '../../store/reducers';

export const REDUCER_KEY__Register = "register";
export const ROUTE_NAME__REGISTER = "register";

export default (store) => ({
  path : 'register',
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      const Register = require('./containers/Register.container').default;
      const reducer = require('./reducers/Register.reducer').default;
      injectReducer(store, { key: REDUCER_KEY__Register, reducer });
      cb(null, Register)
    }, 'register')
  }
})
