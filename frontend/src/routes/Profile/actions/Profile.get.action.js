/**
 * Created by zekar on 8/12/2017.
 */
import {getProfile} from "../../../middleware/services/profileService";
export const PROFILE_GET_REQUEST = 'PROFILE_GET/REQUEST';
export const PROFILE_GET_SUCCESS = 'PROFILE_GET/SUCCESS';
export const PROFILE_GET_ERROR = 'PROFILE_GET/ERROR';

function request() {
  return {
    type: PROFILE_GET_REQUEST,
    isFetching: true
  }
}

function success(profile) {
  return {
    type: PROFILE_GET_SUCCESS,
    isFetching: false,
    profile
  }
}

function error() {
  return {
    type: PROFILE_GET_ERROR,
    isFetching: false
  }
}

export default function action() {
  return async (dispatch, getState) => {
    dispatch(request());
    try {
      const response = await getProfile(dispatch);
      dispatch(success(response));
    } catch (e) {
      dispatch(error(e));
    }
  }
}
