import {redirectableFetch, redirectableJsonFetch} from "../fetchHelpers";

const BASE_ROUTE = "http://localhost:3010/api/notes";

export {
  getList,
  get,
  post,
  put,
  remove
}

async function getList() {
  return await redirectableJsonFetch(`${BASE_ROUTE}`);
}

async function get(id) {
  return await redirectableJsonFetch(`${BASE_ROUTE}/${id}`);
}

async function post(item) {
  return await redirectableJsonFetch(`${BASE_ROUTE}`, {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: mapModel(item)
  });
}

async function put(id, item) {
  const url = `${BASE_ROUTE}/${id}`;
  const options = {
    method: 'PUT',
    headers: {'Content-Type': 'application/json'},
    body: mapModel(item)
  };
  return await redirectableJsonFetch(url, options);
}


async function remove(id) {
  return await redirectableJsonFetch(`${BASE_ROUTE}/${id}`, {
    method: 'DELETE',
    headers: {'Content-Type': 'application/json'}
  });
}

//////////////
///// Privates

function mapModel(item) {
  return JSON.stringify({
    "text": item.text,
    "isChecked": item.isChecked
  });
}
