import HomeView from './components/HomeView'

// Sync route definition
export const ROUTE_NAME__HOME = "/";
export default {
  component : HomeView
}
