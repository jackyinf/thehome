import React, {Component} from 'react';
import { Form } from 'semantic-ui-react'

// src: https://stackoverflow.com/questions/44535840/unable-to-make-the-checkbox-work-with-redux-form-and-react-semantic-ui

class CustomCheckbox extends Component {
  render() {
    const { input: { value, onChange }, custom: { mainLabel, label } } = this.props;
    return (
      <Form.Field>
        <label>{mainLabel}</label>
        <Form.Checkbox defaultChecked={!!value}
                       label={label}
                       onChange={(e, data) => onChange(data.checked)} />
      </Form.Field>

    )
  }
}

export default CustomCheckbox;
