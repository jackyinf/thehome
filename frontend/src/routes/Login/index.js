import { injectReducer } from '../../store/reducers';

export const REDUCER_KEY__LOGIN = "login";
export const ROUTE_NAME__LOGIN = "login";

export default (store) => ({
  path : ROUTE_NAME__LOGIN,
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      const Login = require('./containers/Login.container').default;
      const reducer = require('./reducers/Login.reducer').default;
      injectReducer(store, { key: REDUCER_KEY__LOGIN, reducer });
      cb(null, Login)
    }, 'login')
  }
})
