import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button, Checkbox, Modal, Form, Divider } from "semantic-ui-react";
import {CREATE_EDIT_FORM__NOTES} from "../index";
import {CustomTextInput, CustomCheckbox} from "../../../components/CustomFormInputs";
import {reduxForm, Field} from "redux-form";

class EditNoteModalComponent extends Component {
  render() {
    const {open, handleCancel, handleConfirm} = this.props;

    return (
      <Modal closeOnDimmerClick={false} closeIcon={true} open={open} onClose={handleCancel}>
        <Modal.Header>Edit note</Modal.Header>
        <Modal.Content image>
          <Form style={{"width": "100%"}} onSubmit={handleConfirm}>
            <Form.Group>
              <Field name="text" component={CustomTextInput} custom={{label: "Text"}} />
              <Field name="isChecked" component={CustomCheckbox} custom={{label: "Is checked"}} />
            </Form.Group>

            <Divider className="divider-hack" />

            <div className="float-right">
              <Button.Group>
                <Button negative onClick={handleCancel}>Cancel</Button>
                <Button.Or />
                <Form.Button positive type='submit'>Save</Form.Button>
              </Button.Group>
            </div>
          </Form>
        </Modal.Content>
      </Modal>
    )
  }
}

EditNoteModalComponent.propTypes = {
  open: PropTypes.bool.isRequired,
  handleCancel: PropTypes.func.isRequired,
  handleConfirm: PropTypes.func.isRequired,
};

EditNoteModalComponent = reduxForm({form: CREATE_EDIT_FORM__NOTES})(EditNoteModalComponent);

export default EditNoteModalComponent;
