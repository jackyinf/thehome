/**
 * Created by zekar on 8/12/2017.
 */
import {connect} from 'react-redux';
import {register, gotoLogin} from '../actions';
import Register from '../components/Register.component';
import {REDUCER_KEY__Register} from "..";

const mapDispatchToProps = {
  register,
  gotoLogin
};

const mapStateToProps = (state) => ({
  isFetching: state[REDUCER_KEY__Register].isFetching
});

export default connect(mapStateToProps, mapDispatchToProps)(Register)
