/**
 * Created by zekar on 8/12/2017.
 */
import {connect} from 'react-redux';
import {get, startEdit, endEdit, cancelEdit} from '../actions';
import Profile from '../components/Profile.component';
import {REDUCER_KEY__PROFILE} from "../index";

const mapDispatchToProps = {
  get,
  startEdit,
  endEdit,
  cancelEdit
};

const mapStateToProps = (state) => {
  const currentState = state[REDUCER_KEY__PROFILE];
  return {
    isFetching: currentState.isFetching,
    editInProgress: currentState.editInProgress,
    profile: currentState.profile
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
