import {connect} from 'react-redux';
import Map from '../components/Map.component';

const mapDispatchToProps = {};

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Map)
