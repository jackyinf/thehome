import {initialize} from "redux-form";
import {get} from "../../../middleware/services/notesService";
import {CREATE_EDIT_FORM__NOTES} from "../index";

export const NOTES_START_EDIT_REQUEST = 'NOTES_START_EDIT/REQUEST';
export const NOTES_START_EDIT_SUCCESS = 'NOTES_START_EDIT/SUCCESS';
export const NOTES_START_EDIT_ERROR = 'NOTES_START_EDIT/ERROR';

function request() {
  return {
    type: NOTES_START_EDIT_REQUEST,
    isFetching: true
  }
}

function success(id) {
  return {
    type: NOTES_START_EDIT_SUCCESS,
    isFetching: false,
    editInProgress: true,
    id
  }
}

function error() {
  return {
    type: NOTES_START_EDIT_ERROR,
    isFetching: false
  }
}

export default function action(id) {
  return async (dispatch) => {
    dispatch(request());
    try {
      const response = await get(id);
      dispatch(initialize(CREATE_EDIT_FORM__NOTES, response));
      dispatch(success(id));
    } catch (e) {
      dispatch(error(e));
      throw e;
    }
  }
}
