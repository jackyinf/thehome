import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {BootstrapTable, TableHeaderColumn} from "react-bootstrap-table";
import {Button, Checkbox, Confirm, Icon, Segment, Dimmer, Loader} from "semantic-ui-react";
import EditNoteModalComponent from "./Notes.item.form.component";

class NotesComponent extends Component {

  constructor() {
    super();
    this.state = {editModalOpen: false};
  }

  checkboxFormatter = (value) => {
    return (
      <text>
        <Checkbox disabled={true} checked={value} />
      </text>
    );
  };
  buttonFormatter = (value, row) => {
    return (
      <text>
        <Button color="blue" onClick={e => this.props.editBegin(row.id)}>Edit</Button>
        <Button color="red" onClick={e => this.props.itemDeleteBegin(row.id)}>Delete</Button>
      </text>)
  };

  componentDidMount() {
    this.props.getList();
  }

  render() {
    const {isFetching, items, editModal, deleteModal} = this.props;
    const {createBegin, editEnd, editCancel, itemDeleteConfirm, itemDeleteCancel} = this.props;

    const {itemId: deletingItemId} = deleteModal;

    const options = {
      onSortChange: this.props.onSortChange
    };

    return (
      <div>
        <Segment>
          <Button positive icon onClick={createBegin}>
            <Icon name="add circle" /> &nbsp;
            Create new notes
          </Button>
        </Segment>

        <Segment>
          <Dimmer active={isFetching}>
            <Loader />
          </Dimmer>

          <BootstrapTable data={ items }
                          remote={ true }
                          exportCSV={ true }
                          options={ options }>
            <TableHeaderColumn dataField='id' isKey={ true }>ID</TableHeaderColumn>
            <TableHeaderColumn dataField='text' dataSort={ true }>Text</TableHeaderColumn>
            <TableHeaderColumn dataField='isChecked' dataFormat={this.checkboxFormatter} >Is checked</TableHeaderColumn>
            <TableHeaderColumn dataFormat={ this.buttonFormatter } >Actions</TableHeaderColumn>
          </BootstrapTable>

          <EditNoteModalComponent open={editModal.open}
                                  handleCancel={editCancel}
                                  handleConfirm={editEnd}
          />

          <Confirm open={deleteModal.open}
                   onCancel={itemDeleteCancel}
                   onConfirm={e => itemDeleteConfirm(deletingItemId)} />
        </Segment>
      </div>
    );
  }
}

NotesComponent.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  items: PropTypes.array.isRequired,
  editModal: PropTypes.object.isRequired,
  deleteModal: PropTypes.object.isRequired,

  getList: PropTypes.func.isRequired,

  createBegin: PropTypes.func.isRequired,
  editBegin: PropTypes.func.isRequired,
  editEnd: PropTypes.func.isRequired,
  editCancel: PropTypes.func.isRequired,

  itemDeleteBegin: PropTypes.func.isRequired,
  itemDeleteConfirm: PropTypes.func.isRequired,
  itemDeleteCancel: PropTypes.func.isRequired,
};
NotesComponent.defaultProps = {};

export default NotesComponent;
