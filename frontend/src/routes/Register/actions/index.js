/**
 * Created by zekar on 8/12/2017.
 */

import register from "./Register.register.action";
import gotoLogin from "./Register.gotoLogin.action";

export {
  register,
  gotoLogin
}
