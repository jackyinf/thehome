// We only need to import the modules necessary for initial render
import CoreLayout from '../layouts/PageLayout/PageLayout.container'
import Home from './Home'
import LoginRoute from './Login'
import RegisterRoute from './Register'
import NotesRoute from './Notes'
import ProfileRoute from './Profile'
import MapRoute from './Map'

import {updateRouteVisibilites} from "../actions";

/*  Note: Instead of using JSX, we recommend using react-router
    PlainRoute objects to build route definitions.   */

export const createRoutes = (store) => {
  initialize(store);

  return {
    path        : '/',
    component   : CoreLayout,
    indexRoute  : Home,
    childRoutes : [
      LoginRoute(store),
      RegisterRoute(store),
      NotesRoute(store),
      ProfileRoute(store),
      MapRoute(store)
    ]
  };
};

/*  Note: childRoutes can be chunked or otherwise loaded programmatically
    using getChildRoutes with the following signature:

    getChildRoutes (location, cb) {
      require.ensure([], (require) => {
        cb(null, [
          // Remove imports!
          require('./Counter').default(store)
        ])
      })
    }

    However, this is not necessary for code-splitting! It simply provides
    an API for async route definitions. Your code splitting should occur
    inside the route `getComponent` function, since it is only invoked
    when the route exists and matches.
*/

export default createRoutes;

function initialize(store) {
  store.dispatch(updateRouteVisibilites());
}
