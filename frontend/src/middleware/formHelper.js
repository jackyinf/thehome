export {
  extractFormValues,
  extractFormValuesFromCurrentState
}

function extractFormValues(getState, formName) {
  const state = getState();
  if (!state)
    throw "state does not exist";
  return extractFormValuesFromCurrentState(state, formName);
}

function extractFormValuesFromCurrentState(state, formName) {
  const formState = state["form"];
  if (!formState)
    throw "redux-form is not registered";
  const currentFormState = formState[formName];
  if (!currentFormState)
    throw `form ${formName} is not initialized`;

  return currentFormState.values;
}
