import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';

class LogoutComponent extends Component {
  render() {
    const {logout, isFetching} = this.props;
    return (
      <div>
        <h3>You are currently logged in</h3>
        <Button loading={isFetching} color="red" onClick={logout}>Logout</Button>
      </div>
    );
  }
}

LogoutComponent.propTypes = {
  isFetching: PropTypes.bool.isRequired,

  logout: PropTypes.func.isRequired
};
LogoutComponent.defaultProps = {};

export default LogoutComponent;
