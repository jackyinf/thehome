import React from "react";

const Authorization = (WrappedComponent, allowedRoles) => {
  return class extends React.Component {
    constructor(props) {
      super(props);

      // In this case the user is hardcoded, but it could be loaded from anywhere.
      // Redux, MobX, RxJS, Backbone...
      const lock = JSON.parse(localStorage.getItem('home'));
      if (lock) {
        this.state = {
          user: {
            name: lock.name,
            role: lock.role
          }
        }
      } else {
        this.state = {
          user: {
            name: 'guest',
            role: ''
          }
        }
      }

    }

    render() {
      const {role} = this.state.user;
      if (allowedRoles.includes(role) || allowedRoles.length === 0 && this.state.user.name !== 'guest') {
        return <WrappedComponent {...this.props} />
      } else {
        return <h1>You have no rights to view this page</h1>
      }
    }
  }
};

export default Authorization;
