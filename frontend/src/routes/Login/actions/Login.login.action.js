/**
 * Created by zekar on 8/12/2017.
 */
import {login} from "../../../middleware/services/authServices";
import {updateRouteVisibilites} from "../../../actions";

export const LOGIN_REQUEST = 'LOGIN/REQUEST';
export const LOGIN_SUCCESS = 'LOGIN/SUCCESS';
export const LOGIN_ERROR = 'LOGIN/ERROR';

function request() {
  return {
    type: LOGIN_REQUEST,
    isFetching: true
  }
}

function success() {
  return {
    type: LOGIN_SUCCESS,
    isFetching: false
  }
}

function error() {
  return {
    type: LOGIN_ERROR,
    isFetching: false
  }
}

export default function action(formData) {
  return async (dispatch, getState) => {
    dispatch(request());
    try {
      formData.set("email", "jevgeni.rum@gmail.com");
      formData.set("password", "123456");
      const response = await login(formData);
      dispatch(updateRouteVisibilites());
      dispatch(success(response));
    } catch (e) {
      dispatch(error(e));
    }
  }
}
