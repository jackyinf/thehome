/**
 * Created by zekar on 8/19/2017.
 */
import {connect} from 'react-redux';
import PageLayoutComponent from './PageLayout.component';

const mapDispatchToProps = {};

const mapStateToProps = (state) => {
  const globalState = state["global"];

  return {
    routeVisibilities: globalState.routeVisibilities
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PageLayoutComponent)
