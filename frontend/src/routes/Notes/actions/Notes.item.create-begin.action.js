import {initialize} from "redux-form";
import {CREATE_EDIT_FORM__NOTES} from "../index";

export const NOTES_START_CREATE_REQUEST = 'NOTES_START_CREATE/REQUEST';
export const NOTES_START_CREATE_SUCCESS = 'NOTES_START_CREATE/SUCCESS';
export const NOTES_START_CREATE_ERROR = 'NOTES_START_CREATE/ERROR';

function request() {
  return {
    type: NOTES_START_CREATE_REQUEST,
    isFetching: true
  }
}

function success() {
  return {
    type: NOTES_START_CREATE_SUCCESS,
    isFetching: false,
    createInProgress: true
  }
}

function error() {
  return {
    type: NOTES_START_CREATE_ERROR,
    isFetching: false
  }
}

export default function action() {
  return async (dispatch) => {
    dispatch(request());
    try {
      dispatch(initialize(CREATE_EDIT_FORM__NOTES, {}));
      dispatch(success());
    } catch (e) {
      dispatch(error(e));
    }
  }
}
