/**
 * Created by zekar on 8/12/2017.
 */

import {LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_ERROR} from "../actions/Login.login.action";
import {LOGOUT_REQUEST, LOGOUT_SUCCESS, LOGOUT_ERROR} from "../actions/Login.logout.action";

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [LOGIN_REQUEST]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),
  [LOGIN_SUCCESS]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),
  [LOGIN_ERROR]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),

  [LOGOUT_REQUEST]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),
  [LOGOUT_SUCCESS]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),
  [LOGOUT_ERROR]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching})
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {isFetching: false};
export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state
}
