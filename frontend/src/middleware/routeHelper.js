/**
 * Created by zekar on 8/19/2017.
 */

import {ROUTE_NAME__PROFILE} from "../routes/Profile";
import {ROUTE_NAME__REGISTER} from "../routes/Register";
import {ROUTE_NAME__HOME} from "../routes/Home";
import {ROUTE_NAME__LOGIN} from "../routes/Login";
import {ROUTE_NAME__NOTES} from "../routes/Notes";
import {ROUTE_NAME__MAP} from "../routes/Map/index";
import { browserHistory } from 'react-router'

import {isAllowedToSeeRoute, isLoggedIn} from "./authHelpers";

export {
  onEnterProtected,
  navigateTo,
  getAllRoutes,

  ROUTE_NAME__PROFILE,
  ROUTE_NAME__REGISTER,
  ROUTE_NAME__HOME,
  ROUTE_NAME__LOGIN,
  ROUTE_NAME__NOTES
}

////////////////////////////
// Privates

function onEnterProtected(routeName, nextState, replace) {
  if (!isAllowedToSeeRoute(routeName)) {
    replace({ nextPathname: nextState.location.pathname, pathname: "login" }); // TODO: env base url
  }
}

function navigateTo(routeName) {
  browserHistory.push(routeName);
}

function getAllRoutes(routeVisibilities) {
  let loginRoute = {
    route: ROUTE_NAME__LOGIN,
    isVisible: routeVisibilities.login,
    label: "Login",
    icon: "sign in",
    name: "login"
  };
  let logoutRoute = {
    route: ROUTE_NAME__LOGIN,
    isVisible: routeVisibilities.login,
    label: "Logout",
    icon: "sign out",
    name: "logout"
  };
  return [
    {
      route: ROUTE_NAME__HOME,
      isVisible: true,
      label: "Home",
      icon: "home",
      name: "home",
      isIndex: true
    },
    isLoggedIn() ? logoutRoute : loginRoute,
    {
      route: ROUTE_NAME__REGISTER,
      isVisible: routeVisibilities.register,
      label: "Register",
      icon: "add user",
      name: "register"
    },
    {
      route: ROUTE_NAME__PROFILE,
      isVisible: routeVisibilities.profile,
      label: "Profile",
      icon: "user circle",
      name: "profile"
    },
    {
      route: ROUTE_NAME__NOTES,
      isVisible: routeVisibilities.notes,
      label: "Notes",
      icon: "window maximize",
      name: "notes"
    },
    {
      route: ROUTE_NAME__MAP,
      isVisible: routeVisibilities.map,
      label: "Some Map",
      icon: "world",
      name: "map"
    }
  ];
}
