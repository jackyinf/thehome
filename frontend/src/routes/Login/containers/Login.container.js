import {connect} from 'react-redux';
import {login, logout} from '../actions';
import Login from '../components/LoginLogout.component';
import {getToken} from "../../../middleware/authHelpers";
import {REDUCER_KEY__LOGIN} from "../index";

const mapDispatchToProps = {login, logout};

const mapStateToProps = (state) => ({
  isLoggedIn: !!getToken(),
  isFetching: state[REDUCER_KEY__LOGIN].isFetching
});

export default connect(mapStateToProps, mapDispatchToProps)(Login)
