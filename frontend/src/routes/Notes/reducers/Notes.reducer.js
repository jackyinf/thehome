/**
 * Created by zekar on 8/12/2017.
 */
import {NOTES_GET_LIST_REQUEST, NOTES_GET_LIST_SUCCESS, NOTES_GET_LIST_ERROR} from "../actions/Notes.get-items.action";
import {
  NOTE_DELETE_OPEN_REQUEST,
  NOTE_DELETE_OPEN_SUCCESS,
  NOTE_DELETE_OPEN_ERROR
} from "../actions/Notes.item.delete-begin.action";
import {
  NOTE_DELETE_CONFIRM_REQUEST,
  NOTE_DELETE_CONFIRM_SUCCESS,
  NOTE_DELETE_CONFIRM_ERROR,
  NOTE_DELETE_CONFIRM_CANCEL
} from "../actions/Notes.item.delete-confirm.action";
import {
  NOTES_START_CREATE_REQUEST,
  NOTES_START_CREATE_SUCCESS,
  NOTES_START_CREATE_ERROR
} from "../actions/Notes.item.create-begin.action";
import {
  NOTES_START_EDIT_REQUEST,
  NOTES_START_EDIT_SUCCESS,
  NOTES_START_EDIT_ERROR
} from "../actions/Notes.item.edit-begin.action";
import {
  NOTES_END_EDIT_REQUEST,
  NOTES_END_EDIT_SUCCESS,
  NOTES_END_EDIT_ERROR,
  NOTES_END_EDIT_CANCEL
} from "../actions/Notes.item.submit.action";

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  // Get actions
  [NOTES_GET_LIST_REQUEST]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),
  [NOTES_GET_LIST_SUCCESS]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching, items: action.items}),
  [NOTES_GET_LIST_ERROR]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),

  // Create and edit actions
  [NOTES_START_CREATE_REQUEST]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),
  [NOTES_START_CREATE_SUCCESS]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching, editModal: {...state.editModal, open: action.createInProgress}}),
  [NOTES_START_CREATE_ERROR]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),
  [NOTES_START_EDIT_REQUEST]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),
  [NOTES_START_EDIT_SUCCESS]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching, editModal: {...state.editModal, open: action.editInProgress}}),
  [NOTES_START_EDIT_ERROR]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),
  [NOTES_END_EDIT_REQUEST]: (state, action) => ({...state, type: action.type, editModal: {...state.editModal, isFetching: action.isFetching}}),
  [NOTES_END_EDIT_SUCCESS]: (state, action) => ({...state, type: action.type, editModal: {...state.editModal, isFetching: action.isFetching, open: action.editInProgress}}),
  [NOTES_END_EDIT_ERROR]: (state, action) => ({...state, type: action.type, editModal: {...state.editModal, isFetching: action.isFetching}}),
  [NOTES_END_EDIT_CANCEL]: (state, action) => ({...state, type: action.type, editModal: {...state.editModal, isFetching: action.isFetching, open: action.editInProgress}}),

  // Delete actions
  [NOTE_DELETE_OPEN_REQUEST]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),
  [NOTE_DELETE_OPEN_SUCCESS]: (state, action) => ({...state, type: action.type, deleteModal: {isFetching: action.isFetching, open: action.open, itemId: action.itemId}}),
  [NOTE_DELETE_OPEN_ERROR]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),
  [NOTE_DELETE_CONFIRM_REQUEST]: (state, action) => ({...state, type: action.type, deleteModal: {...state.deleteModal, isFetching: action.isFetching}}),
  [NOTE_DELETE_CONFIRM_SUCCESS]: (state, action) => ({...state, type: action.type, deleteModal: {isFetching: action.isFetching, open: action.open, temId: null}}),
  [NOTE_DELETE_CONFIRM_ERROR]: (state, action) => ({...state, type: action.type, deleteModal: {...state.deleteModal, isFetching: action.isFetching}}),
  [NOTE_DELETE_CONFIRM_CANCEL]: (state, action) => ({...state, type: action.type, deleteModal: {...state.deleteModal, isFetching: action.isFetching, open: action.open, temId: null}}),
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  items: [],
  isFetching: false,
  editModal: { // TODO: rename to createEditModal or formModal or...
    isFetching: false,
    open: false
  },
  deleteModal: {
    isFetching: false,
    open: false,
    itemId: null
  }
};
export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state
}
