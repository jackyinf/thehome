import React, {Component} from 'react';
import { Card, Icon, Image, Button, Header, Modal, Divider, Form } from 'semantic-ui-react'

class CustomTextInput extends Component {
  render() {
    const { input: { value, onChange }, custom: { label } } = this.props;
    return (
      <Form.Field>
        <label>{label}</label>
        <Form.Input placeholder={label} value={value} onChange={onChange} />
      </Form.Field>
    )
  }
}

export default CustomTextInput;
