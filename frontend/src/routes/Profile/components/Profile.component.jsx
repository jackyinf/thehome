import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Card, Icon, Image, Button, Modal} from 'semantic-ui-react'
import Matthew from "../assets/matthew.png";
import "./Profile.component.scss";
import ProfileForm from "./Profile.form.component";

class ProfileComponents extends Component {

  componentDidMount() {
    this.props.get();
  }

  render() {
    const {editInProgress, startEdit, cancelEdit, endEdit, profile} = this.props;

    return (
      <div>
        <Card>
          <Image src={Matthew} />
          <Card.Content>
            <Card.Header>
              {profile.username}
            </Card.Header>
          </Card.Content>
          <Card.Content extra>
            <a>
              <Icon name='mail' />
              {profile.email}
            </a>
            <Button circular icon='settings' floated="right" onClick={e => startEdit()} />
          </Card.Content>
        </Card>

          <Modal open={editInProgress}>
            <Modal.Header>Edit a profile</Modal.Header>
            <Modal.Content image>
              <ProfileForm
                handleCancel={cancelEdit}
                handleSubmit={endEdit}
              />
            </Modal.Content>
          </Modal>

      </div>
    );
  }
}

ProfileComponents.propTypes = {
  editInProgress: PropTypes.bool.isRequired,
  profile: PropTypes.object.isRequired,

  get: PropTypes.func.isRequired,
  startEdit: PropTypes.func.isRequired,
  endEdit: PropTypes.func.isRequired,
  cancelEdit: PropTypes.func.isRequired,
};
ProfileComponents.defaultProps = {};

export default ProfileComponents;
