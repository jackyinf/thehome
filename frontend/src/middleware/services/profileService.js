/**
 * Created by zekar on 8/19/2017.
 */
import {getToken} from "../authHelpers";
import {redirectableFetch} from "../fetchHelpers";

export {
  getProfile
}

async function getProfile(dispatch) {
  const token = getToken();
  if (!token) {
    throw "not logged in";
  }

  const httpResponse = await redirectableFetch(`http://localhost:3010/api/Users/${token.userId}?access_token=${token.id}`, null, dispatch);
  return httpResponse.json();
}
