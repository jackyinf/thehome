import {getProfile} from "../../../middleware/services/profileService";
import {EDIT_FORM__PROFILE} from "../index";
import {initialize} from "redux-form";

export const PROFILE_START_EDIT_REQUEST = 'PROFILE_START_EDIT/REQUEST';
export const PROFILE_START_EDIT_SUCCESS = 'PROFILE_START_EDIT/SUCCESS';
export const PROFILE_START_EDIT_ERROR = 'PROFILE_START_EDIT/ERROR';

function request() {
  return {
    type: PROFILE_START_EDIT_REQUEST,
    isFetching: true
  }
}

function success() {
  return {
    type: PROFILE_START_EDIT_SUCCESS,
    isFetching: false,
    editInProgress: true
  }
}

function error() {
  return {
    type: PROFILE_START_EDIT_ERROR,
    isFetching: false
  }
}

export default function action() {
  return async (dispatch) => {
    dispatch(request());
    try {
      const response = await getProfile(dispatch);
      dispatch(initialize(EDIT_FORM__PROFILE, {username: response.username, email: response.email}));
      dispatch(success());
    } catch (e) {
      dispatch(error(e));
    }
  }
}
