'use strict';

module.exports = function(app) {
  /*
   * The `app` object provides access to a variety of LoopBack resources such as
   * models (e.g. `app.models.YourModelName`) or data sources (e.g.
   * `app.datasources.YourDataSource`). See
   * http://docs.strongloop.com/display/public/LB/Working+with+LoopBack+objects
   * for more info.
   */
  var User = app.models.User;
  var Role = app.models.Role;
  var RoleMapping = app.models.RoleMapping;


  User.create([
    {username: 'admin', email: 'jevgeni.rum@gmail.com', password: '123456'}
  ], function(err, users) {
    if (err) return console.error(err);
    // Create the admin role
    Role.create({
      name: 'admin'
    }, function(err, role) {
      if (err) return console.error(err);
      // Give Admin user the admin role
      role.principals.create({
        principalType: RoleMapping.USER,
        principalId: users[0].id
      }, function(err, principal) {
        if (err) return console.error(err);
        // done!
        RoleMapping.belongsTo(User);
        User.hasMany(RoleMapping, {foreignKey: 'principalId'});
        Role.hasMany(User, {through: RoleMapping, foreignKey: 'roleId'});
      });
    });
  });
};
