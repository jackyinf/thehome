/**
 * Created by zekar on 8/12/2017.
 */

import {
  PROFILE_GET_REQUEST,
  PROFILE_GET_SUCCESS,
  PROFILE_GET_ERROR
} from "../actions/Profile.get.action";
import {
  PROFILE_START_EDIT_ERROR,
  PROFILE_START_EDIT_REQUEST,
  PROFILE_START_EDIT_SUCCESS
} from "../actions/Profile.start-edit.action";
import {
  PROFILE_END_EDIT_CANCEL,
  PROFILE_END_EDIT_ERROR,
  PROFILE_END_EDIT_REQUEST,
  PROFILE_END_EDIT_SUCCESS
} from "../actions/Profile.end-edit.action";

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [PROFILE_GET_REQUEST]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),
  [PROFILE_GET_SUCCESS]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching, profile: action.profile}),
  [PROFILE_GET_ERROR]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),

  [PROFILE_START_EDIT_REQUEST]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),
  [PROFILE_START_EDIT_SUCCESS]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching, editInProgress: action.editInProgress}),
  [PROFILE_START_EDIT_ERROR]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),

  [PROFILE_END_EDIT_REQUEST]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),
  [PROFILE_END_EDIT_SUCCESS]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching, editInProgress: action.editInProgress, profile: action.profile}),
  [PROFILE_END_EDIT_ERROR]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),
  [PROFILE_END_EDIT_CANCEL]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching, editInProgress: action.editInProgress})
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isFetching: false,
  editInProgress: false,
  profile: {
    username: '',
    email: ''
  }
};
export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state
}
