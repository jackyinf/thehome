import {get} from "../../../middleware/services/notesService";

export const NOTE_DELETE_OPEN_REQUEST = 'NOTE_DELETE_OPEN/REQUEST';
export const NOTE_DELETE_OPEN_SUCCESS = 'NOTE_DELETE_OPEN/SUCCESS';
export const NOTE_DELETE_OPEN_ERROR = 'NOTE_DELETE_OPEN/ERROR';

function request() {
  return {
    type: NOTE_DELETE_OPEN_REQUEST,
    isFetching: true,
  }
}

function success(itemId) {
  return {
    type: NOTE_DELETE_OPEN_SUCCESS,
    isFetching: false,
    open: true,
    itemId
  }
}

function error() {
  return {
    type: NOTE_DELETE_OPEN_ERROR,
    isFetching: false
  }
}

export default function action(id) {
  return async (dispatch) => {
    dispatch(request());
    try {
      const item = await get(id);
      dispatch(success(item.id));
    } catch (e) {
      dispatch(error(e));
      throw e;
    }
  }
}

