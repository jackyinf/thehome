/**
 * Created by zekar on 8/19/2017.
 */
import {isAllowedToSeeRoute} from "../middleware/authHelpers";
import {
  ROUTE_NAME__PROFILE,
  ROUTE_NAME__LOGIN,
  ROUTE_NAME__NOTES,
  ROUTE_NAME__REGISTER
} from "../middleware/routeHelper";

export const GLOBAL__SET_NAV_LINK_VISIBILITY = "GLOBAL/SET_NAV_LINK_VISIBILITY";

export default function updateRouteVisibilites() {
  const login = isAllowedToSeeRoute(ROUTE_NAME__LOGIN);
  const register = isAllowedToSeeRoute(ROUTE_NAME__REGISTER);
  const profile = isAllowedToSeeRoute(ROUTE_NAME__PROFILE);
  const notes = isAllowedToSeeRoute(ROUTE_NAME__NOTES);
  const map = isAllowedToSeeRoute(ROUTE_NAME__NOTES);

  return {
    type: GLOBAL__SET_NAV_LINK_VISIBILITY,
    routeVisibilities: {
      home: true,
      login,
      register,
      profile,
      notes,
      map
    }
  }
}
