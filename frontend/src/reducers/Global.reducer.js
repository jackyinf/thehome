/**
 * Created by zekar on 8/19/2017.
 */
import {GLOBAL__SET_NAV_LINK_VISIBILITY} from "../actions/Global.update-route-visibilities.action";

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [GLOBAL__SET_NAV_LINK_VISIBILITY]:
    (state, action) => ({...state, type: action.type, routeVisibilities: action.routeVisibilities})
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  routeVisibilities: {
    home: true,
    login: false,
    register: false,
    profile: false,
    notes: false,
    map: false
  }
};
export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state
}
