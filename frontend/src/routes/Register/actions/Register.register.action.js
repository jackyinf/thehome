/**
 * Created by zekar on 8/12/2017.
 */
import {register} from "../../../middleware/services/authServices";
import {updateRouteVisibilites} from "../../../actions";

export const REGISTER_REQUEST = 'REGISTER/REQUEST';
export const REGISTER_SUCCESS = 'REGISTER/SUCCESS';
export const REGISTER_ERROR = 'REGISTER/ERROR';

function request() {
  return {
    type: REGISTER_REQUEST,
    isFetching: true
  }
}

function success() {
  return {
    type: REGISTER_SUCCESS,
    isFetching: false
  }
}

function error() {
  return {
    type: REGISTER_ERROR,
    isFetching: false
  }
}

export default function action(formData) {
  return async (dispatch, getState) => {
    dispatch(request());
    try {
      const response = await register(formData);
      dispatch(updateRouteVisibilites());
      dispatch(success(response));
    } catch (e) {
      dispatch(error(e));
    }
  }
}
