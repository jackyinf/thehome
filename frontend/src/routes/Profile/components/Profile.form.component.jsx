import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {reduxForm, Field} from "redux-form";
import {EDIT_FORM__PROFILE} from "../index";
import {CustomTextInput} from "../../../components/CustomFormInputs";
import { Button, Divider, Form } from 'semantic-ui-react'

class ProfileFormComponent extends Component {
  render() {
    const { handleCancel, handleSubmit } = this.props;

    return (
      <Form style={{"width": "100%"}} onSubmit={handleSubmit}>

        <Form.Group>
          <Field name="username" component={CustomTextInput} custom={{label: "Username"}} />
          <Field name="email" component={CustomTextInput} custom={{label: "Email"}} />
        </Form.Group>

        <Divider className="divider-hack" />

        <div className="float-right">
          <Button.Group>
            <Button onClick={handleCancel}>Cancel</Button>
            <Button.Or />
            <Form.Button positive type='submit'>Save</Form.Button>
          </Button.Group>
        </div>
      </Form>
    )
  }
}

ProfileFormComponent.propTypes = {
  handleCancel: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
};
ProfileFormComponent.defaultProps = {};

ProfileFormComponent = reduxForm({form: EDIT_FORM__PROFILE})(ProfileFormComponent);

export default ProfileFormComponent;
