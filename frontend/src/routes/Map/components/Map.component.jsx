import React, {Component} from 'react';
import PropTypes from 'prop-types';
import MapGL from 'react-map-gl';
import DeckGL, {LineLayer} from 'deck.gl';

const MAPBOX_ACCESS_TOKEN = 'pk.eyJ1Ijoic3R5bGVob3BwZXIiLCJhIjoiY2o2amhxNmlkMTczejJ3cW1wNTF0N2wzZyJ9.onHx9-JjPAiYLEpkWUWyCw';

const data = [
  {sourcePosition: [-122.41669, 37.7853], targetPosition: [-122.41669, 37.781]}
];

// Viewport settings that is shared between mapbox and deck.gl
const viewport = {
  width: 500,
  height: 500,
  longitude: -100,
  latitude: 40.7,
  zoom: 3,
  pitch: 0,
  bearing: 0
};

class MapComponent extends Component {
  render() {
    return (
      <div>
        <MapGL {...viewport} mapboxApiAccessToken={MAPBOX_ACCESS_TOKEN}>
          <DeckGL {...viewport} layers={[
            new LineLayer({id: 'line-layer', data})
          ]} />
        </MapGL>
      </div>
    );
  }
}

MapComponent.propTypes = {};
MapComponent.defaultProps = {};

export default MapComponent;
