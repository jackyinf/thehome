import {post, put} from "../../../middleware/services/notesService";
import {extractFormValuesFromCurrentState} from "../../../middleware/formHelper";
import {CREATE_EDIT_FORM__NOTES, REDUCER_KEY__NOTES} from "../index";
import getList from "./Notes.get-items.action";

export const NOTES_END_EDIT_REQUEST = 'NOTES_END_EDIT/REQUEST';
export const NOTES_END_EDIT_SUCCESS = 'NOTES_END_EDIT/SUCCESS';
export const NOTES_END_EDIT_ERROR = 'NOTES_END_EDIT/ERROR';
export const NOTES_END_EDIT_CANCEL = 'NOTES_END_EDIT/CANCEL';

function request() {
  return {
    type: NOTES_END_EDIT_REQUEST,
    isFetching: true
  }
}

function success() {
  return {
    type: NOTES_END_EDIT_SUCCESS,
    isFetching: false,
    editInProgress: false
  }
}

function error() {
  return {
    type: NOTES_END_EDIT_ERROR,
    isFetching: false
  }
}

export function cancel() {
  return {
    type: NOTES_END_EDIT_CANCEL,
    isFetching: false,
    editInProgress: false
  }
}

export default function action() {
  return async (dispatch, getState) => {
    dispatch(request());
    const state = getState();
    const currentFormValues = extractFormValuesFromCurrentState(state, CREATE_EDIT_FORM__NOTES);
    const id = currentFormValues.id;

    try {
      !isNaN(id) ? await put(id, currentFormValues) : await post(currentFormValues);
      dispatch(getList());
      dispatch(success());
    } catch (e) {
      dispatch(error(e));
      throw e;
    }
  }
}
