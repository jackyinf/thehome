import {remove} from "../../../middleware/services/notesService";
import getItems from "./Notes.get-items.action";

export const NOTE_DELETE_CONFIRM_REQUEST = 'NOTE_DELETE_CONFIRM/REQUEST';
export const NOTE_DELETE_CONFIRM_SUCCESS = 'NOTE_DELETE_CONFIRM/SUCCESS';
export const NOTE_DELETE_CONFIRM_ERROR = 'NOTE_DELETE_CONFIRM/ERROR';
export const NOTE_DELETE_CONFIRM_CANCEL = 'NOTE_DELETE_CONFIRM/CANCEL';

function request() {
  return {
    type: NOTE_DELETE_CONFIRM_REQUEST,
    isFetching: true
  }
}

function success() {
  return {
    type: NOTE_DELETE_CONFIRM_SUCCESS,
    isFetching: false,
    open: false
  }
}

function error() {
  return {
    type: NOTE_DELETE_CONFIRM_ERROR,
    isFetching: false
  }
}

export function cancel() {
  return {
    type: NOTE_DELETE_CONFIRM_CANCEL,
    isFetching: false
  }
}

export default function action(id) {
  return async (dispatch) => {
    dispatch(request());
    try {
      await remove(id);
      dispatch(getItems());
      dispatch(success());
    } catch (e) {
      dispatch(error(e));
      throw e;
    }
  }
}
