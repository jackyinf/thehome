/**
 * Created by zekar on 8/19/2017.
 */

import {redirectToLogin, unsetToken} from "./authHelpers";
import {updateRouteVisibilites} from "../actions";

export {
  redirectableFetch,
  redirectableJsonFetch
}

async function redirectableFetch(url, options = null, dispatch = null) {
  const httpResponse = await fetch(url, options);

  if (!httpResponse.ok) {
    switch (httpResponse.status) {
      case 401:
        console.error("Unauthorized");
        unsetToken();
        redirectToLogin();
        if (dispatch && dispatch instanceof Function)
          dispatch(updateRouteVisibilites());
    }
  }

  return httpResponse;
}

async function redirectableJsonFetch(url, options = {}, dispatch = null) {
  options = options || {};
  options.headers = options.headers || {};
  options.headers['Content-Type'] = options.headers['Content-Type'] || 'application/json';

  const response = await redirectableFetch(url, options, dispatch).then(response => response.json());

  if (response.error)
    throw response.error;

  return response;
}
