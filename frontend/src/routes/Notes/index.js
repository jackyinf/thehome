/**
 * Created by zekar on 8/12/2017.
 */
import {injectReducer} from '../../store/reducers';

export const REDUCER_KEY__NOTES = "notes";
export const ROUTE_NAME__NOTES = "notes";
export const CREATE_EDIT_FORM__NOTES = "notes_create_edit";

export default (store) => ({
  path: 'notes',
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      const notes = require('./containers/Notes.container').default;
      const reducer = require('./reducers/Notes.reducer').default;
      injectReducer(store, {key: REDUCER_KEY__NOTES, reducer});
      cb(null, notes)
    }, "notes")
  }
})
