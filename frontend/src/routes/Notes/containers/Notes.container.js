/**
 * Created by zekar on 8/12/2017.
 */
import {connect} from 'react-redux';
import {
  getItems,
  itemDeleteBegin,
  itemDeleteConfirm,
  itemDeleteCancel,
  itemCreateBegin,
  itemEditBegin,
  itemSubmitConfirm,
  itemSubmitCancel
} from '../actions';
import Notes from '../components/Notes.component';
import {REDUCER_KEY__NOTES} from "../index";

const mapDispatchToProps = {
  getList: getItems,

  onSortChange: (p1, p2) => {console.log("on sort change", p1, p2)},

  createBegin: itemCreateBegin,
  editBegin: itemEditBegin,
  editEnd: itemSubmitConfirm,
  editCancel: itemSubmitCancel,

  itemDeleteBegin: itemDeleteBegin,
  itemDeleteConfirm: itemDeleteConfirm,
  itemDeleteCancel: itemDeleteCancel
};

const mapStateToProps = (state) => {
  const currentState = state[REDUCER_KEY__NOTES];

  return {
    isFetching: currentState.isFetching,
    items: currentState.items,

    editModal: currentState.editModal,
    deleteModal: currentState.deleteModal
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Notes)
