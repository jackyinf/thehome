/**
 * Created by zekar on 8/12/2017.
 */

import get from "./Profile.get.action";
import startEdit from "./Profile.start-edit.action";
import endEdit, {cancel as cancelEdit} from "./Profile.end-edit.action";

export {
  get,
  startEdit,
  endEdit,
  cancelEdit
}
