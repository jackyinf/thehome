/**
 * Created by zekar on 8/19/2017.
 */
import {getToken, setToken, unsetToken} from "../authHelpers";

export {
  login,
  logout,
  register
}

async function login(formData) {
  const response = await fetch("http://localhost:3010/api/users/login?include=user", {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      "email": formData.get('email'),
      "password": formData.get('password'),
      "ttl": 86400 // TODO: remember me
    })
  }).then(response => response.json());

  if (response.error)
    throw response.error;

  setToken(response.id, response.userId, 'admin'); // TODO: get role from response
  return response;
}

async function logout() {
  const token = getToken();
  if (!token) {
    throw "you are already logged out";
  }

  const httpResponse = await fetch(`http://localhost:3010/api/Users/logout?access_token=${token.id}`, {method: "POST"});
  unsetToken();
  return httpResponse;
}

async function register(formData) {
  const email = formData.get('email');
  const password = formData.get('password');

  const response = await fetch("http://localhost:3010/api/users", {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ email, password })
  }).then(resp => resp.json());

  if (response.error)
    throw response.error;

  await login(formData);
}
