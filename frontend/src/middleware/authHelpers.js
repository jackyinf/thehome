/**
 * Created by zekar on 8/19/2017.
 */

import {
  ROUTE_NAME__PROFILE,
  ROUTE_NAME__REGISTER,
  ROUTE_NAME__NOTES,
  ROUTE_NAME__LOGIN
} from "./routeHelper";
import _ from "lodash";
import { browserHistory } from 'react-router'

export {
  setToken,
  unsetToken,
  getToken,

  isAllowedToSeeRoute,
  redirectToLogin,
  isLoggedIn
}

//////////////////////////////////////////////

function setToken(tokenId, userId, roleName) {
  localStorage.setItem("home", JSON.stringify({
    userId: userId,
    id: tokenId,
    role: roleName
  }));
}

function unsetToken() {
  localStorage.removeItem("home");
}

function getToken() {
  return JSON.parse(localStorage.getItem('home'));
}

function isAllowedToSeeRoute(routeName) {
  const requiredRoles = getRolesFor(routeName);
  if (requiredRoles instanceof Array && requiredRoles.length === 0)
    return true;
  const availableRoles = getCurrentUserRoles(routeName);
  const matchedRoles = _.intersection(requiredRoles, availableRoles);
  return matchedRoles.length > 0;
}

function isLoggedIn() {
  return !!getToken();
}

/////////////////////
// Privates

function getRolesFor(routeName) {
  switch (routeName) {
    case ROUTE_NAME__PROFILE:
    case ROUTE_NAME__NOTES:
      return ['manager', 'admin'];
    case ROUTE_NAME__REGISTER:
      return ['guest'];
    default:
      return [];
  }
}

function getCurrentUserRoles() {
  const token = getToken();
  if (!token || !token.role)
    return ['guest'];
  return [token.role];
}

function redirectToLogin() {
  browserHistory.push(ROUTE_NAME__LOGIN);
}
