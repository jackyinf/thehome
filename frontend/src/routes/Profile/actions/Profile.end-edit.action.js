import {EDIT_FORM__PROFILE, REDUCER_KEY__PROFILE} from "../index";

export const PROFILE_END_EDIT_REQUEST = 'PROFILE_END_EDIT/REQUEST';
export const PROFILE_END_EDIT_SUCCESS = 'PROFILE_END_EDIT/SUCCESS';
export const PROFILE_END_EDIT_ERROR = 'PROFILE_END_EDIT/ERROR';
export const PROFILE_END_EDIT_CANCEL = 'PROFILE_END_EDIT/CANCEL';

function request() {
  return {
    type: PROFILE_END_EDIT_REQUEST,
    isFetching: true
  }
}

function success(profile) {
  return {
    type: PROFILE_END_EDIT_SUCCESS,
    isFetching: false,
    editInProgress: false,
    profile
  }
}

function error() {
  return {
    type: PROFILE_END_EDIT_ERROR,
    isFetching: false
  }
}

export function cancel() {
  return {
    type: PROFILE_END_EDIT_CANCEL,
    isFetching: false,
    editInProgress: false
  }
}

export default function action() {
  return async (dispatch, getState) => {
    const editForm = getState()["form"][EDIT_FORM__PROFILE];
    const profile = editForm.values;

    dispatch(request());
    try {
      // TODO: do a PUT request to the database
      dispatch(success(profile));
    } catch (e) {
      dispatch(error(e));
    }
  }
}
