/**
 * Created by zekar on 8/12/2017.
 */
import {REGISTER_REQUEST, REGISTER_SUCCESS, REGISTER_ERROR} from "../actions/Register.register.action";

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [REGISTER_REQUEST]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),
  [REGISTER_SUCCESS]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching}),
  [REGISTER_ERROR]: (state, action) => ({...state, type: action.type, isFetching: action.isFetching})
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {isFetching: false};
export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state
}
