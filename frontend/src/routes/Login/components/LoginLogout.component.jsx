import React, {Component} from 'react';
import PropTypes from 'prop-types';

import LoginComponent from "./Login.component";
import LogoutComponent from "./Logout.component";

class LoginLogoutComponent extends Component {
  render() {
    const {isLoggedIn, isFetching, login, logout} = this.props;

    return (
      <div>
        {isLoggedIn
          ? <LogoutComponent logout={logout} isFetching={isFetching}/>
          : <LoginComponent login={login} isFetching={isFetching}/>
        }
      </div>
    );
  }
}

LoginLogoutComponent.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  login: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired
};
LoginLogoutComponent.defaultProps = {};

export default LoginLogoutComponent;
