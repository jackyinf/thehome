import React from 'react'
import PropTypes from 'prop-types'
import './PageLayout.scss'
import { Sidebar, Menu, Icon } from 'semantic-ui-react'
import {navigateTo, getAllRoutes} from "../../middleware/routeHelper";
import { Link, IndexLink } from "react-router";

class SidebarLeftOverlay extends React.Component {
  render() {
    const {routeVisibilities} = this.props;
    const routes = getAllRoutes(routeVisibilities);

    return (
      <div>
        <Sidebar as={Menu} animation='slide along' width='thin' visible={true} icon='labeled' vertical inverted>
          {routes.filter(item => item.isVisible).map((item, key) => (
            <Menu.Item key={key} name={item.name} onClick={e => navigateTo(item.route)} >
              <Icon name={item.icon} />
              {item.isIndex // TODO: get rid of warning of nested a tag: "Warning: validateDOMNesting(...): <a> cannot appear as a descendant of <a>. See SidebarLeftOverlay > MenuItem > a > ... > Link > a."
                ? <IndexLink to={item.route} activeClassName="page-layout__nav-item--active">{item.label}</IndexLink>
                : <Link to={item.route} activeClassName="page-layout__nav-item--active">{item.label}</Link>}
            </Menu.Item>
          ))}
        </Sidebar>
      </div>
    )
  }
}

SidebarLeftOverlay.propTypes = {
  routeVisibilities: PropTypes.object.isRequired
};

class PageLayoutSimple extends React.Component {
  render() {
    const {children, routeVisibilities} = this.props;

    return (
      <div>

        <SidebarLeftOverlay routeVisibilities={routeVisibilities}/>
        <div style={{"margin": "20px 0 0 150px"}}> {/* TODO: solve 150px hack by aligning SideBar and content container side-by-side without any overlay*/}
          <div className="container">
            {children}
          </div>
        </div>

      </div>
    )
  }
}

PageLayoutSimple.propTypes = {
  children: PropTypes.node,
  routeVisibilities: PropTypes.object.isRequired
};


export default PageLayoutSimple
