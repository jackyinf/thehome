/**
 * Created by zekar on 8/12/2017.
 */

import login from "./Login.login.action";
import logout from "./Login.logout.action";

export {
  login,
  logout
}
