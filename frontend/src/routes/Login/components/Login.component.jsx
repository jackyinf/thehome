import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Button, Checkbox, Form } from 'semantic-ui-react';

class LoginComponent extends Component {

  constructor(props) {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.login(new FormData(event.target));
  }

  render() {
    const {isFetching} = this.props;

    return (
    <Form loading={isFetching} onSubmit={this.handleSubmit}>
      <Form.Field>
        <label>Email</label>
        <input name="email" placeholder='Email' />
      </Form.Field>
      <Form.Field>
        <label>Password</label>
        <input name="password" type="password" placeholder='Password' />
      </Form.Field>
      <Form.Field>
        <Checkbox name="rememberMe" label='Remember me' />
      </Form.Field>
      <Button type='submit'>Sign in</Button>
    </Form>
    );
  }
}

LoginComponent.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  login: PropTypes.func.isRequired,
};
LoginComponent.defaultProps = {};

export default LoginComponent;
