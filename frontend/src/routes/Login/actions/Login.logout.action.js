/**
 * Created by zekar on 8/12/2017.
 */
import {logout} from "../../../middleware/services/authServices";
import {updateRouteVisibilites} from "../../../actions";

export const LOGOUT_REQUEST = 'LOGOUT/REQUEST';
export const LOGOUT_SUCCESS = 'LOGOUT/SUCCESS';
export const LOGOUT_ERROR = 'LOGOUT/ERROR';

function request() {
  return {
    type: LOGOUT_REQUEST,
    isFetching: true
  }
}

function success() {
  return {
    type: LOGOUT_SUCCESS,
    isFetching: false
  }
}

function error() {
  return {
    type: LOGOUT_ERROR,
    isFetching: false
  }
}

export default function action() {
  return async (dispatch, getState) => {
    dispatch(request());
    try {
      await logout();
      dispatch(updateRouteVisibilites());
      dispatch(success());
    } catch (e) {
      dispatch(error(e));
    }
  }
}
