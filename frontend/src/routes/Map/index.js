
export const ROUTE_NAME__MAP = "map";

export default (store) => ({
  path: 'map',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const map = require('./containers/Map.container').default;
      cb(null, map)
    }, "map");
  }
})
