/**
 * Created by zekar on 8/12/2017.
 */
export const NOTES_GET_LIST_REQUEST = 'NOTES_GET_LIST/REQUEST';
export const NOTES_GET_LIST_SUCCESS = 'NOTES_GET_LIST/SUCCESS';
export const NOTES_GET_LIST_ERROR = 'NOTES_GET_LIST/ERROR';

import {getList} from "../../../middleware/services/notesService";

function request() {
  return {
    type: NOTES_GET_LIST_REQUEST,
    isFetching: true
  }
}

function success(items) {
  return {
    type: NOTES_GET_LIST_SUCCESS,
    isFetching: false,
    items: items
  }
}

function error() {
  return {
    type: NOTES_GET_LIST_ERROR,
    isFetching: false
  }
}

export default function action() {
  return async (dispatch) => {
    dispatch(request());
    try {
      const items = await getList();
      dispatch(success(items));
    } catch (e) {
      dispatch(error(e));
      throw e;
    }
  }
}
