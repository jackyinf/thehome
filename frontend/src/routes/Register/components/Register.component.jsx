/**
 * Created by zekar on 8/12/2017.
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import "./Register.component.scss";
import { Button, Checkbox, Form } from 'semantic-ui-react';

class RegisterComponent extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.register(new FormData(event.target));
  }

  render() {
    const {isFetching} = this.props;

    return (
      <Form loading={isFetching} onSubmit={this.handleSubmit}>
        <Form.Field>
          <label>Email</label>
          <input name="email" placeholder='Email' />
        </Form.Field>
        <Form.Field>
          <label>First Name</label>
          <input name="firstname" placeholder='First Name' />
        </Form.Field>
        <Form.Field>
          <label>Last Name</label>
          <input name="lastname" placeholder='Last Name' />
        </Form.Field>
        <Form.Field>
          <label>Password</label>
          <input type="password" placeholder='Password' />
        </Form.Field>
        <Form.Field>
          <Checkbox label='I agree to the Terms and Conditions' />
        </Form.Field>
        <Button type='submit'>Submit</Button>
      </Form>
    );
  }
}

RegisterComponent.propTypes = {
  isFetching: PropTypes.bool.isRequired,

  register: PropTypes.func.isRequired,
  gotoLogin: PropTypes.func.isRequired
};
RegisterComponent.defaultProps = {};

export default RegisterComponent;
