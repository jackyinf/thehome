/**
 * Created by zekar on 8/12/2017.
 */
import {injectReducer} from '../../store/reducers';
import {onEnterProtected} from "../../middleware/routeHelper";

export const REDUCER_KEY__PROFILE = "profile";
export const ROUTE_NAME__PROFILE = "profile";
export const EDIT_FORM__PROFILE = "profile_edit";

export default (store) => ({
  path: 'profile',
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      const profile = require('./containers/Profile.container').default;
      const reducer = require('./reducers/Profile.reducer').default;
      injectReducer(store, {key: REDUCER_KEY__PROFILE, reducer});
      cb(null, profile)
    }, "profile")
  },
  onEnter: (...params) => onEnterProtected(ROUTE_NAME__PROFILE, ...params)
})
